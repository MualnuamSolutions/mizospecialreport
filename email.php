<?php 

// Process email
$emailErr = null;
$name = $_POST["full_name"];
$mobile = $_POST["mobile"];
$transaction_id = $_POST["transaction_id"];
$email = $_POST["email"];
$issue = '';

$error = false;

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$emailErr = "Please use correct email address."; 
	$error = true;
}
if($transaction_id == "") {
	$transactionError = "Your need to provide transaction reference number."; 
	$error = true;
}

if(!$error) {
	$to = "contact@mizospecialreport.com";
	$subject = "New Order - Mizo Special Report Monthly Magazine";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// More headers
	$headers .= 'From: Mizo Special Report <magazine@mizospecialreport.com>' . "\r\n";

	$message = "
	<html>
		<head>
			<title>New Order - Mizo Special Report Monthly Magazine</title>
		</head>
		<body>
			<h3>Order Detail</h3>
			<table border='1' cellpadding='10' style='border: 1px solid #111111;border-collapse: collapse;'>
				<tr>
					<th scope='row' align='right'>Name</th>
					<td>{$name}</td>
				</tr>
				<tr>
					<th scope='row' align='right'>Email</th>
					<td>{$email}</td>
				</tr>
				<tr>
					<th scope='row' align='right'>Mobile</th>
					<td>{$mobile}</td>
				</tr>
				<tr>
					<th scope='row' align='right'>Transaction Reference Number</th>
					<td>{$transaction_id}</td>
				</tr>
			</table>
		</body>
	</html>
	";

	$ok = mail($to, $subject, $message, $headers);

	$dbuser = 'mizosvqs_dbuser';
	$dbpass = 'kBvE1=32VE[P';
	$db = 'mizosvqs_magazine';

		// Create connection
	$conn = new mysqli('localhost', $dbuser, $dbpass, $db);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "INSERT INTO subscription (id, name, email, mobile, issue, transaction_reference_number, created)
	VALUES (NULL, '{$name}', '{$email}', '{$mobile}', '{$issue}', '{$transaction_id}', NOW())";

	$success = false;

	if ($conn->query($sql) === TRUE) {
	    $success = true;
	} else {
	    echo "Error: " . $sql . "<br>" . $conn->error;
	}

	$conn->close();
}


include('header.php');
?>	

	<div class="centercontent">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<?php if($emailErr): ?>
					<div class="alert alert-danger">
						<p><?php echo $emailErr; ?></p>
						<p><?php echo $transactionError; ?></p>
						<p>Please <a href='/#buy-online'>click here</a> to try again</p>
					</div>
					<?php endif; ?>

					<div class="alert alert-success">
						<h4><i class="icon-checkmark4"></i> Thank you</h4>
						<p>Thank you for purchasing Mizo Special Report Monthly Magazine. Your request has been sent successfully. We will get back to you as soon as possible.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include('footer.php'); ?>	