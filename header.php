<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Mizo Special Report</title>
	<link href='http://fonts.googleapis.com/css?family=Anton|Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="bootstrap-3.3.4-dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="icomoon.css">	
	<link rel="stylesheet" href="style.css">	
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
				<h1>Mizo Special Report</h1>
			</div>
		</div>
	</div>