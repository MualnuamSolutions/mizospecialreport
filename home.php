<?php include('header.php'); ?>	

	<div class="centercontent">
		<div class="container">
			<div class="row promo" id="promo-video">
				<div class="the-icon">
					<i class="icon-video-camera"></i>
				</div>
				<h3>MIZO SPECIAL REPORT MONTHLY MAGAZINE</h3>
				<h2>PROMO VIDEO</h2>
				<div class="the-video">
					<?php include('video.php'); ?>
				</div>
			</div>

			<div class="row writer" id="people">
				<div class="the-icon">
					<i class="icon-pencil6"></i>
				</div>
				<h3>MINUNG</h3>
				<h2>PATHUM</h2>
				<div class="the-writer">
					<div class="row">
						<div class="col-md-4 first">
							<img src="images/mafaa.png" alt="">
							<p>Mafa-a Hauhnar</p>
						</div>
						<div class="col-md-4 second">
							<img src="images/vana.png" alt="">
							<p>Vanneihthanga Vanchhawng</p>
						</div>
						<div class="col-md-4 third">
							<img src="images/tansanga.png" alt="">
							<p>FAMILIA Fanai Laltansanga</p>
						</div>
					</div>
				</div>
			</div>

			<div class="row buy" id="where-to-buy">
				<div class="the-icon">
					<i class="icon-cart4"></i>
				</div>
				<h3>WHERE TO</h3>
				<h2>BUY</h2>
				<div class="the-stores">
					<div class="row">
						<div class="col-md-4 first">
							<i class="icon-store"></i>
							<p>LIANCHHUNGI BOOK STORE</p>
						</div>
						<div class="col-md-4 second">
							<img width="80px" height="80px" src="images/mizostore.png" alt="">
							<p><a target="_blank" href="http://www.mizostore.com/profile/publisher/165/mizo-special-report">BUY ONLINE AT MIZOSTORE</a></p>
						</div>
						<div class="col-md-4 third">
							<i class="icon-office"></i>
							<p>MIZO SPECIAL REPORT OFFICE</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

<?php include('footer.php'); ?>